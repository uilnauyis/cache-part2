﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace CacheServer
{
    public class CacheController : ApiController
    {
        private static HttpClient _client = new HttpClient();
        public static HashSet<string> _cachedBlockHash = new HashSet<string>();

        [HttpGet]
        public async Task<IHttpActionResult> ListFiles()
        {
            try
            {
                var response = await _client.GetAsync("http://localhost:9000/server/ListFiles");
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    return Ok("Server response error");
                }
                var responseContentStr = await response.Content.ReadAsStringAsync();
                var fileNames = JsonConvert.DeserializeObject<List<string>>(responseContentStr);
                return Ok(fileNames);
            }
            catch (Exception err)
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                    "Exception happens while attempting to process the file"));
            }
        }

        [HttpGet]
        public async Task<IHttpActionResult> DownloadFile([FromUri]string fileName = null)
        {
            byte[] result;
            List<byte[]> receivedBlocks = new List<byte[]>();
            try
            {
                var response = await _client.GetAsync($"http://localhost:9000/server/DownloadFile?fileName={fileName}");
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    return Ok("Server response error");
                }
                receivedBlocks = await response.Content.ReadAsAsync<List<byte[]>>();

                double percentage = await ReplaceHashsWithBlocksOrSaveBlocks(receivedBlocks);

                result = receivedBlocks.Aggregate((combinedBytes, bytes) =>
                {
                    var newCombinedBytes = new byte[combinedBytes.Length + bytes.Length];
                    Array.Copy(combinedBytes, newCombinedBytes, combinedBytes.Length);
                    Array.Copy(bytes, 0, newCombinedBytes, combinedBytes.Length, bytes.Length);
                    return newCombinedBytes;
                });

                await UpdateLog(fileName, percentage);
            }
            catch (Exception err)
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                    "Exception happens while attempting to process the file"));
            }

            return Ok(result);
        }

        private async Task<double> ReplaceHashsWithBlocksOrSaveBlocks(List<byte[]> content)
        {
            double originalSize = 0;
            double cachedSize = 0;
            await Task.Run(async () => {
                for (int contentIndex = 0; contentIndex < content.Count; contentIndex++)
                { 
                    string hashString;
                    if (content[contentIndex].Length == 16)
                    {
                        hashString = Util.BytesToHexString(content[contentIndex]);
                        if (_cachedBlockHash.Contains(hashString))
                        {
                            content[contentIndex] = await Util.ReadFile(hashString);
                            cachedSize += content[contentIndex].Length;
                            originalSize += content[contentIndex].Length;
                            continue;
                        }
                    }
                    hashString = await Hash(content[contentIndex]);
                    await SaveBlock(hashString, content[contentIndex]);
                    originalSize += content[contentIndex].Length;
                    _cachedBlockHash.Add(hashString);
                }
            });
            return cachedSize / originalSize * 100;
        }

        private async Task SaveBlock(string hashString, byte[] content)
        {
            await Task.Run(() =>
            {
                using (System.IO.FileStream fs = System.IO.File.Create($"{Util.CACHE_STORAGE_PATH}/{hashString}"))
                {
                    for (int i = 0; i < content.Length; i++)
                    {
                        fs.WriteByte(content[i]);
                    }
                }
            });
        }

        private async Task<string> Hash(byte[] block)
        {
            return await Task.Run(() =>
            {
                MD5 hashAlgorithm = MD5.Create();
                byte[] hash = hashAlgorithm.ComputeHash(block);
                string stringifiedHash = Util.BytesToHexString(hash);
                return stringifiedHash;
            });
        }

        private async Task UpdateLog(string fileName, double percentage)
        {
            using (var writer = new StreamWriter(Util.LOG_PATH, true))
            {
                await writer.WriteLineAsync($"user request: file {fileName} at {DateTime.Now}");
                await writer.WriteLineAsync($"response: {percentage}% of file {fileName} was constructed with the cached data");
            }
        }
    }
}

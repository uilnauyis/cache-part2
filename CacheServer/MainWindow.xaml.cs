﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CacheServer
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private FileSystemWatcher filesWatcher;
		private FileSystemWatcher logWatcher;

		private Thread serverThread;

		public MainWindow()
		{
			InitializeComponent();
			Util.CreateStorage();
			Util.CreateLog();
			serverThread = new Thread(Server.Start);
			startLogWatcher();
			serverThread.Start();
		}

		protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
		{
			serverThread.Abort();
			base.OnClosing(e);
		}

		private void startLogWatcher()
		{
			logWatcher = new FileSystemWatcher();
			logWatcher.Path = System.IO.Path.GetDirectoryName(Util.LOG_PATH);
			logWatcher.Filter = System.IO.Path.GetFileName(Util.LOG_PATH);
			logWatcher.NotifyFilter = NotifyFilters.LastWrite;
			logWatcher.Changed += new FileSystemEventHandler(OnLogChanged);
			logWatcher.EnableRaisingEvents = true;
		}

		private void OnFilesChanged()
		{
			this.Dispatcher.Invoke(() =>
			{
				List<Item> avilableFilesItems = new List<Item>();
				foreach (string file in Directory.EnumerateFiles(Util.CACHE_STORAGE_PATH))
				{
					avilableFilesItems.Add(new Item()
					{
						FileName = System.IO.Path.GetFileName(file)
					});
				};
				avilableFiles.ItemsSource = avilableFilesItems;
			});
		}

		private void OnLogChanged(object source, FileSystemEventArgs e)
		{
			this.Dispatcher.Invoke(() =>
			{
				MemoryStream responseStream = new MemoryStream();
				logs.Text = File.ReadAllText(Util.LOG_PATH);
			});
            OnFilesChanged();
        }

		private async void Button_Click(object sender, RoutedEventArgs e)
		{
            using (HttpClient client = new HttpClient())
			{
				client.GetAsync("http://localhost:9000/server/ClearCache").Wait();
			}
			Util.ClearCache();
			await UpdateLogForClearingCache();
			OnLogChanged(null, null);
		}

		public async void ListBoxItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{

			ListBoxItem itemObject = (ListBoxItem)sender;
			Item item = (Item)itemObject.Content;
			string fileName = item.FileName;

			byte[] fileContent = await Util.ReadFile(fileName);
			string fileContentAsString = Util.BytesToHexString(fileContent, true);

			BlockContent blockContentWindow = new BlockContent(fileContentAsString);
			blockContentWindow.Show();
		}

		private async Task UpdateLogForClearingCache()
		{
			using (var writer = new StreamWriter(Util.LOG_PATH, true))
			{
				await writer.WriteLineAsync("Clear cache");
			}
		}
	}

	class Item
	{
		public string FileName { get; set; }
	}
}

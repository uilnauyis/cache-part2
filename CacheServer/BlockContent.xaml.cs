﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CacheServer
{
    /// <summary>
    /// Interaction logic for BlockContent.xaml
    /// </summary>
    public partial class BlockContent : Window
    {
        public BlockContent(string blockContentString)
        {
            InitializeComponent();
            BlockContentString.Text = blockContentString;
        }
    }
}
